const express = require('express');

const BooksRoute = express.Router();

const BooksModel = require('../model/Books');

BooksRoute.get('/', async(req, res) => {
  try {
    let books = await BooksModel.find();
    // res.send(books);
    res.render('index', {
      books
    });
  } catch(e) {
    console.log(e)
  }
})

BooksRoute.post('/addbook', async(req, res) => {
  console.log(req.body)
  try {
    let newBook = BooksModel({
      title: req.body.title,
      author: req.body.author,
      genre: req.body.genre,
    })

    newBook = await newBook.save();
    res.redirect('/')
  } catch(e) {
    console.log(e)
  }

})

BooksRoute.delete('/deletebook/:id', async(req, res) => {
  try{
    let deletedBook = await BooksModel.findByIdAndDelete(req.params.id)
    res.redirect('/')
  } catch(e) {
    console.log(e)
  }
})

BooksRoute.put('/editbook/:id', async(req, res) => {
  try {
    let book = await BooksModel.findById(req.params.id);

    if(!book){
      return res.status(404).send(`Book can't be found!`);
    }

    let condition = {_id: req.params.id};
    let updates = {
      title: req.body.title,
      author: req.body.author,
      genre: req.body.genre
    }

    let updatedBook= await BooksModel.findByIdAndUpdate(condition, updates, {new: true});
    res.redirect('/')
  } catch (e) {
    console.log(e)
  }
})

module.exports = BooksRoute;