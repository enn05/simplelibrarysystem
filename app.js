const express = require('express');
const path = require('path');
const bodyParser = require('body-parser')
const app = express();
const port = 3000;
const mongoose = require('mongoose');
const methodOverride = require('method-override')
const cors = require('cors');

// connect to db
const databaseUrl = process.env.DATABASE_URL || "mongodb+srv://admin:batch46092519@b46mongostack-zfef5.mongodb.net/simpleLibrarySystem?retryWrites=true&w=majority";
mongoose
 .connect(
   databaseUrl,
   {
     useNewUrlParser: true,
     useUnifiedTopology: true,
     useFindAndModify: false,
     useCreateIndex: true
   }
 )
 .then(() => {
   console.log("Remote Database Connection Established");
 });

// app.use(require("body-parser").text());

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs')


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(methodOverride('_method'))


app.listen(port, () => {
  console.log(`App listening at port ${port}`)
})

// Books API
const books = require('./routes/books_route');
app.use('/', books);